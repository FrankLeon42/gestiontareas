package com.sistemaDeGestionDeTareas.servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.sistemaDeGestionDeTareas.modelo.Usuario;
import com.sistemaDeGestionDeTareas.repositorio.RepositorioUsuario;

@Service
public class UsuarioServicio implements UserDetailsService {
	
	@Autowired
	private RepositorioUsuario repoUsuario;

	public void guardar(String userName, String contraseña) {
		String contraseñaIncriptada = new BCryptPasswordEncoder().encode(contraseña);
		Usuario u = new Usuario(userName, contraseñaIncriptada);
		repoUsuario.save(u);
	}

//	public void editarUsuario(Long id, String nombre, String contraseña) {
//		Optional<Usuario> usuario = repoUsuario.findById(id);
//		usuario.get().editar(nombre, contraseña);
//	}
//	public Usuario buscarUsuario(Long id) {
//		return repoUsuario.buscarPorId(id);
//	}
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario u = repoUsuario.buscarPorusername(username);
	
		if (u == null) {
			return null;
		}
		
		List<GrantedAuthority> permisos = new ArrayList<>();

		GrantedAuthority p1 = new SimpleGrantedAuthority("ROLE_" + u.getRol().toString());
		permisos.add(p1);

		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

		HttpSession session = attr.getRequest().getSession(true);
		session.setAttribute("usuariosession", u);

		return new User(u.getUsername(), u.getContraseña(), permisos);

	}

}
