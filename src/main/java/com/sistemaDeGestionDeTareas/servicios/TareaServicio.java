package com.sistemaDeGestionDeTareas.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemaDeGestionDeTareas.modelo.Tarea;
import com.sistemaDeGestionDeTareas.modelo.Usuario;
import com.sistemaDeGestionDeTareas.repositorio.RepositorioTarea;
import com.sistemaDeGestionDeTareas.repositorio.RepositorioUsuario;

@Service
public class TareaServicio {
	
	@Autowired
	private RepositorioTarea repoTarea;
	
	@Autowired
	private UsuarioServicio usuarioServicio;

	public void guardarTarea(String titulo, String descripcion, String estado,Usuario usuario) {
//		Usuario usuario = usuarioServicio.buscarUsuario(id);
		
		Tarea tarea = new Tarea(titulo, descripcion, estado,usuario);
		repoTarea.save(tarea);
	}

	public void editar(Long id, String titulo, String descripcion, String estado) {
		Tarea tarea = buscarTarea(id);
		tarea.editar(titulo, descripcion, estado);
		repoTarea.save(tarea);
	}
	public Tarea buscarTarea(Long id) {
		return repoTarea.buscarPorId(id);
	}
	public void eliminar(Long id) {
		repoTarea.deleteById(id);
	}

	public List<Tarea> buscarTareasDeUsuario(Long idUsuario) {
		return repoTarea.buscarTareasDeUsuario(idUsuario);
		
	}

}
