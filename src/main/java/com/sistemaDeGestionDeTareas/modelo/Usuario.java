package com.sistemaDeGestionDeTareas.modelo;



import com.sistemaDeGestionDeTareas.enums.Rol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.EnumType;

@Entity
@Table(name = "usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private Long id;

	private String username;
	private String contraseña;
	
	@Enumerated(EnumType.STRING)
    private Rol rol;

	public Usuario(String username, String contraseña) {
		this.username = username;
		this.contraseña = contraseña;
		this.rol = rol.ADMIN;
	}
	public Usuario() {
	    
	}


//	public void editar(String username, String contraseña) {
//		this.username = username;
//		this.contraseña = contraseña;
//	}

	public String getUsername() {
		return username;
	}

	public String getContraseña() {
		return contraseña;
	}
	
	public Rol getRol() {
        return rol;
    }
	public Long getId() {
		return id;
	}
	
	

}
