package com.sistemaDeGestionDeTareas.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Tarea")
public class Tarea {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tarea")
	private Long id;

	@Column(name = "titulo")
	private String titulo;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "estado")
	private String estado;

	@ManyToOne
	@JoinColumn(name = "id_usuario", nullable = false)
	private Usuario usuario;

	public Tarea(String titulo, String descripcion, String estado, Usuario usuario) {
		this.usuario = usuario;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public Tarea() {
		// Constructor vacío
	}

	public void editar(String titulo, String descripcion, String estado) {
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.estado = estado;

	}

	public Long getId() {
		return id;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getEstado() {
		return estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}
	
	
}
