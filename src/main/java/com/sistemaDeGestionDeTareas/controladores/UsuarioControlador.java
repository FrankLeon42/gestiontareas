package com.sistemaDeGestionDeTareas.controladores;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sistemaDeGestionDeTareas.servicios.UsuarioServicio;

@Controller
@RequestMapping("/usuario")
public class UsuarioControlador {
	@Autowired
	private UsuarioServicio usuarioServicio;

	@PostMapping("/registro")
	public String registrar(@RequestParam String username, @RequestParam String contraseña) {
		usuarioServicio.guardar(username, contraseña);
		return "index.html";
	}
	
	
}
