package com.sistemaDeGestionDeTareas.controladores;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class PortalControlador {

	@GetMapping("/index")
	public String index() {
		return "index.html";
	}

	@GetMapping("/login")
	public String login() {
		return "inicio.html";
	}

	@GetMapping("/registro")
	public String registro() {
		return "registro.html";

	}
	
	@GetMapping("/menu")
	public String menu() {
		return "menu.html";
	}
	

}
