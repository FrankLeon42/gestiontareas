package com.sistemaDeGestionDeTareas.controladores;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sistemaDeGestionDeTareas.modelo.Tarea;
import com.sistemaDeGestionDeTareas.modelo.Usuario;
import com.sistemaDeGestionDeTareas.servicios.TareaServicio;

@Controller
@RequestMapping("/tareas")
public class TareaControlador {

	@Autowired
	TareaServicio tareaServicio;

	@GetMapping("/crear")
	public String crearGet() {
		return "crearTarea.html";
	}

	@PostMapping("/crear")
	public String crear(@RequestParam String titulo, @RequestParam String descripcion, @RequestParam String estado, HttpSession session) {
        Usuario login = (Usuario) session.getAttribute("usuariosession");
		tareaServicio.guardarTarea(titulo, descripcion,estado,login);
		return "crearTarea.html";

	}
	
	@GetMapping("/lista")
	public String verTareas(ModelMap model,HttpSession session) {
		 Usuario login = (Usuario) session.getAttribute("usuariosession");
		 List<Tarea> tareas = tareaServicio.buscarTareasDeUsuario(login.getId()); 
		 model.put("tareas", tareas);
		return "verTareas.html";
	}
	
	@PostMapping("/eliminar/{id}")
	public String borrar(@PathVariable Long id) {
		tareaServicio.eliminar(id);
		return "redirect:/tareas/lista";
	}
	
	
	//editar Tarea 
	
	@GetMapping("/editar/{id}")
	public String editar(ModelMap model,@PathVariable Long id) {
		Tarea tarea = tareaServicio.buscarTarea(id);
		model.put("tarea",tarea);	
		return "editarTarea.html";
	}
	
	@PostMapping("/editar/{id}")
	public String editar(@PathVariable Long id,@RequestParam String titulo, @RequestParam String descripcion,@RequestParam String estado) {
		tareaServicio.editar(id, titulo, descripcion, estado);
		return "redirect:/tareas/lista";
	}

}
