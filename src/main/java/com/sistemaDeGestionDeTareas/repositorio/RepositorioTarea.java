package com.sistemaDeGestionDeTareas.repositorio;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sistemaDeGestionDeTareas.modelo.Tarea;



@Repository
public interface RepositorioTarea extends JpaRepository<Tarea,Long>{
	
	@Query("SELECT t FROM Tarea t WHERE t.usuario.id = :idUsuario")
	public List<Tarea> buscarTareasDeUsuario(@Param("idUsuario") Long idUsuario);
	
	@Query("SELECT t FROM Tarea t WHERE t.id = :id")
	public Tarea buscarPorId(@Param("id") Long id);

}
