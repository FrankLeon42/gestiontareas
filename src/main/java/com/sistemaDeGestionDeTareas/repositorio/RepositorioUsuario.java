package com.sistemaDeGestionDeTareas.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sistemaDeGestionDeTareas.modelo.Usuario;

@Repository
public interface RepositorioUsuario extends JpaRepository<Usuario, Long> {

	@Query("SELECT u FROM Usuario u WHERE u.username LIKE :username")
	public Usuario buscarPorusername(@Param("username") String username);
	
//	@Query("SELECT u FROM Usuario u WHERE u.id_usuario = :id")
//	public Usuario buscarPorId(@Param("id") String id);




}
