package com.sistemaDeGestionDeTareas;

import com.sistemaDeGestionDeTareas.modelo.Tarea;
import com.sistemaDeGestionDeTareas.modelo.Usuario;
import com.sistemaDeGestionDeTareas.repositorio.RepositorioTarea;
import com.sistemaDeGestionDeTareas.servicios.TareaServicio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class TareaServicioTest {
    @Autowired
    private TareaServicio tareaServicio;

    @MockBean
    private RepositorioTarea repositorioTarea;

    @BeforeEach
    public void setUp() {
        // Configurar comportamiento simulado del repositorio antes de cada prueba
        Tarea tarea = new Tarea("Titulo", "Descripción", "En Progreso", new Usuario());
        when(repositorioTarea.buscarPorId(1L)).thenReturn(tarea);
    }

    @Test
    public void testGuardarTarea() {
        Usuario usuario = new Usuario();
        tareaServicio.guardarTarea("Titulo", "Descripción", "En Progreso", usuario);
        verify(repositorioTarea, times(1)).save(any(Tarea.class));
    }

    @Test
    public void testEditar() {
        tareaServicio.editar(1L, "Nuevo Título", "Nueva Descripción", "Finalizada");
        verify(repositorioTarea, times(1)).save(any(Tarea.class));
    }

    @Test
    public void testBuscarTarea() {
        Tarea tarea = tareaServicio.buscarTarea(1L);
        verify(repositorioTarea, times(1)).buscarPorId(1L);
        assertNotNull(tarea);
    }

    @Test
    public void testEliminar() {
        tareaServicio.eliminar(1L);
        verify(repositorioTarea, times(1)).deleteById(1L);
    }
}
